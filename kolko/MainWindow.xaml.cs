﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace kolko
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool Isplayer1turn { get; set; } 

        public int Counter { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            NewGame();
        }


        public void NewGame() 
        {
            Isplayer1turn = false;
            Counter = 0;
            Button_0_0.Content = string.Empty;
            Button_1_0.Content = string.Empty;
            Button_2_0.Content = string.Empty;
            Button_0_1.Content = string.Empty;
            Button_1_1.Content = string.Empty;
            Button_2_1.Content = string.Empty;
            Button_0_2.Content = string.Empty;
            Button_1_2.Content = string.Empty;
            Button_2_2.Content = string.Empty;

            Button_0_0.Background = Brushes.White;
            Button_1_0.Background = Brushes.White;
            Button_2_0.Background = Brushes.White;
            Button_0_1.Background = Brushes.White;
            Button_1_1.Background = Brushes.White;
            Button_2_1.Background = Brushes.White;
            Button_0_2.Background = Brushes.White;
            Button_1_2.Background = Brushes.White;
            Button_2_2.Background = Brushes.White;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Isplayer1turn ^= true;
            Counter++;
            if (Counter > 9)
            {
                NewGame();
                return;
            
            }
                
            var button = sender as Button;
            
            button.Content =  Isplayer1turn ? "O" : "X";

            if (checkisplayerwon()) 
            {
                Counter = 9;
            
            }

            
            
        }

        private bool checkisplayerwon() 
        {
            if(Button_0_0.Content.ToString() != string.Empty && Button_0_0.Content == Button_0_1.Content && Button_0_1.Content == Button_0_2.Content) 
            {
                Button_0_0.Background = Brushes.Red;
                Button_0_1.Background = Brushes.Red;
                Button_0_2.Background = Brushes.Red;
                return true;
            
            
            }
            if (Button_1_0.Content.ToString() != string.Empty && Button_1_0.Content == Button_1_1.Content && Button_1_1.Content == Button_1_2.Content)
            {
                Button_1_0.Background = Brushes.Red;
                Button_1_1.Background = Brushes.Red;
                Button_1_2.Background = Brushes.Red;
                return true;


            }
            if (Button_2_0.Content.ToString() != string.Empty && Button_2_0.Content == Button_2_1.Content && Button_2_1.Content == Button_2_2.Content)
            {
                Button_2_0.Background = Brushes.Red;
                Button_2_1.Background = Brushes.Red;
                Button_2_2.Background = Brushes.Red;
                return true;


            }
            if ( Button_0_0.Content.ToString() != string.Empty && Button_0_0.Content == Button_1_0.Content && Button_1_0.Content == Button_2_0.Content)
            {
                Button_0_0.Background = Brushes.Red;
                Button_1_0.Background = Brushes.Red;
                Button_2_0.Background = Brushes.Red;
                return true;


            }
            if (Button_0_1.Content.ToString() != string.Empty && Button_0_1.Content == Button_1_1.Content && Button_1_1.Content == Button_2_1.Content)
            {
                Button_0_1.Background = Brushes.Red;
                Button_1_1.Background = Brushes.Red;
                Button_2_1.Background = Brushes.Red;
                return true;


            }
            if (Button_0_2.Content.ToString() != string.Empty && Button_0_2.Content == Button_1_2.Content && Button_1_2.Content == Button_2_2.Content)
            {
                Button_0_2.Background = Brushes.Red;
                Button_1_2.Background = Brushes.Red;
                Button_2_2.Background = Brushes.Red;
                return true;


            }
            if (Button_0_0.Content.ToString() != string.Empty && Button_0_0.Content == Button_1_1.Content && Button_1_1.Content == Button_2_2.Content)
            {
                Button_0_0.Background = Brushes.Red;
                Button_1_1.Background = Brushes.Red;
                Button_2_2.Background = Brushes.Red;
                return true;


            }
            if (Button_0_2.Content.ToString() != string.Empty && Button_0_2.Content == Button_1_1.Content && Button_1_1.Content == Button_2_0.Content)
            {
                Button_0_2.Background = Brushes.Red;
                Button_1_1.Background = Brushes.Red;
                Button_2_0.Background = Brushes.Red;
                return true;


            }
            return false;

        }

        
    }
}
